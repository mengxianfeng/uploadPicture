<?php
// +---------------------------------------------------------------------+
// | JadePHP    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Xianfeng <107604242@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | JadePHP                      |
// +---------------------------------------------------------------------+

namespace app\admin\validate;

/**
 * 文章分类验证器
 */
class PrintClass extends AdminBase
{
    
    // 验证规则
    protected $rule =   [
        'name'          => 'require',
        'coupon_price'          => 'require',
        'original_price'          => 'require',
    ];

    // 验证提示
    protected $message  =   [
        'name.require'         => '分类名称不能为空',
        'coupon_price.require'         => '实际销售价格不能为空',
        'original_price.require'         => '原价不能为空',

    ];
    
    // 应用场景
    protected $scene = [
        'edit'  =>  ['describe','original_price','coupon_price'],
    ];
}
