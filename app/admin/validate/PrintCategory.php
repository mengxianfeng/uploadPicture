<?php
// +---------------------------------------------------------------------+
// | JadePHP    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Xianfeng <107604242@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | JadePHP                      |
// +---------------------------------------------------------------------+

namespace app\admin\validate;

/**
 * 文章分类验证器
 */
class PrintCategory extends AdminBase
{
    
    // 验证规则
    protected $rule =   [
        'name'          => 'require',
    ];

    // 验证提示
    protected $message  =   [
        'name.require'         => '分类名称不能为空',
    ];
    
    // 应用场景
    protected $scene = [
        'edit'  =>  ['name'],
    ];
}
