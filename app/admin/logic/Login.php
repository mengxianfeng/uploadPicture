<?php
// +---------------------------------------------------------------------+
// | JadePHP    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Xianfeng <107604242@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | JadePHP                      |
// +---------------------------------------------------------------------+

namespace app\admin\logic;

/**
 * 登录逻辑
 */
class Login extends AdminBase
{
    
    /**
     * 登录处理
     */
    public function loginHandle($username = '', $password = '', $verify = '')
    {
        
        $validate_result = $this->validateLogin->scene('admin')->check(compact('username','password','verify'));
        
        if (!$validate_result) {
            
            return [RESULT_ERROR, $this->validateLogin->getError()];
        }
        //登录逻辑重新定义
        //当前用户是否为admin，非admin用户登录后自动添加角色为18
        $member = $this->logicMember->getMemberInfo(['username' => $username]);
        if ("admin" == $username && !empty($member['password']) && data_md5_key($password) == $member['password']) {
            $this->logicMember->setMemberValue(['id' => $member['id']], TIME_UT_NAME, TIME_NOW);

            $auth = ['member_id' => $member['id'], TIME_UT_NAME => TIME_NOW];

            session('member_info', $member);
            session('member_auth', $auth);
            session('member_auth_sign', data_auth_sign($auth));

            action_log('登录', '登录操作，username：'. $username);

            return [RESULT_SUCCESS, '登录成功', url('index/index')];
            
        } else {
            //登录失败
            $password = md5($password);
            $params = ['account'=>$username,"password"=>$password];
            $result = http_curl("http://www.jingcaiweixinserver.51chongyin.com/v2/login",$params);
            $member_info_curl = json_decode($result);
            if($result){
                //第三方登录成功 -- 查看当前账号是否存在并登录
                if(!empty($member['id'])){
                    $this->logicMember->setMemberValue(['id' => $member['id']], TIME_UT_NAME, TIME_NOW);

                    $auth = ['member_id' => $member['id'], TIME_UT_NAME => TIME_NOW];

                    session('member_info', $member);
                    session('member_auth', $auth);
                    session('member_auth_sign', data_auth_sign($auth));

                    action_log('登录', '登录操作，username：'. $username);

                    return [RESULT_SUCCESS, '登录成功', url('index/index')];
                }else{
                    //添加账号
                    $member['store_id'] = $member_info_curl->currentAdd;
                    $member['nickname'] =  $member_info_curl->AddressInfo->name;
                    $member['username'] =  $username;
                    $member['password'] =  md5($password);
                    $member['mobile'] =  $username;
                    $member_id = $this->logicMember->addMember($member);
                    //添加组别
                    $group_access['member_id'] = $member_id;
                    $group_access['group_id'] = 18;
                    $this->logicMember->addGroup($group_access);
                    $this->logicMember->setMemberValue(['id' => $member_id], TIME_UT_NAME, TIME_NOW);
                    $auth = ['member_id' =>$member_id, TIME_UT_NAME => TIME_NOW];
                    $member[TIME_UT_NAME] = TIME_NOW;
                    session('member_info', $member);
                    session('member_auth', $auth);
                    session('member_auth_sign', data_auth_sign($auth));
                    return [RESULT_SUCCESS, '登录成功', url('index/index')];

                }
            }
            $error = empty($member['id']) ? '用户账号不存在' : '密码输入错误';
            
            return [RESULT_ERROR, $error];
        }
    }
    
    /**
     * 注销当前用户
     */
    public function logout()
    {
        
        clear_login_session();
        
        return [RESULT_SUCCESS, '注销成功', url('login/login')];
    }
    
    /**
     * 清理缓存
     */
    public function clearCache()
    {
        
        \think\Cache::clear();
        
        return [RESULT_SUCCESS, '清理成功', url('index/index')];
    }
}
