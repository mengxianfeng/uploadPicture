<?php
// +---------------------------------------------------------------------+
// | JadePHP    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Xianfeng <107604242@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | JadePHP                      |
// +---------------------------------------------------------------------+

namespace app\admin\controller;
use app\common\model\PrintCategory;

/**
 * 分类控制器
 */
class Category extends AdminBase
{

    /**
     * 分类添加
     */
    public function categoryAdd()
    {
        $type = isset($this->param['type'])?$this->param['type']:PrintCategory::TYPE_ALBUM;
        IS_POST && $this->jump($this->logicCategory->printCategoryEdit($this->param));
        $this->assign("type",$type);
        return $this->fetch('print_category_edit');
    }

    public function classAdd()
    {
        IS_POST && $this->jump($this->logicCategory->printClassEdit($this->param));
        $category_info = $this->logicCategory->getPrintCategoryInfo(['id' => $this->param['category_id']]);
        $this->assign('category_info', $category_info);
        return $this->fetch('print_class_edit');
    }

    /**
     * 分类编辑
     */
    public function categoryEdit()
    {
        $type = isset($this->param['type'])?$this->param['type']:PrintCategory::TYPE_ALBUM;
        IS_POST && $this->jump($this->logicCategory->printCategoryEdit($this->param));
        $this->assign("type",$type);
        $info = $this->logicCategory->getPrintCategoryInfo(['id' => $this->param['id']]);

        $this->assign('info', $info);

        return $this->fetch('print_category_edit');
    }


    public function classEdit()
    {
        IS_POST && $this->jump($this->logicCategory->printClassEdit($this->param));

        $info = $this->logicCategory->getPrintClassInfo(['id' => $this->param['id']]);
        $this->assign('info', $info);

        $category_info = $this->logicCategory->getPrintCategoryInfo(['id' => $info['category_id']]);
        $this->assign('category_info', $category_info);
        return $this->fetch('print_class_edit');
    }


    public function PrintClassList()
    {
        $category_id = isset($this->param['category_id'])?$this->param['category_id']:0;
        $where['category_id'] = $category_id;
        $this->assign('list', $this->logicCategory->getPrintClassList($where));
       // $this->assign("type",PrintCategory::TYPE_ALBUM);
        return $this->fetch('print_class_list');
    }

    /**
     * 文章分类列表
     */
    public function categoryList()
    {

        $type = isset($this->param['type'])?$this->param['type']:PrintCategory::TYPE_ALBUM;
        switch ($type){
            case PrintCategory::TYPE_ALBUM:
                $this->redirect("albumlist");
                break;
            case PrintCategory::TYPE_FRAME:
                $this->redirect("framelist");
                break;
            case PrintCategory::TYPE_CALENDAR:
                $this->redirect("calendarlist");
                break;
            case PrintCategory::TYPE_POSTER:
                $this->redirect("posterlist");
                break;
        }

    }



    public function printlist()
    {
        //http://www.jingcaiweixinserver.51chongyin.com/v2/getPhotoSize?printerShopId=f6396df6-ba75-4739-8ade-e589941cf2ff﻿
        $member_info = session("member_info");
        $params = ['printerShopId'=>$member_info['store_id']];
        $result = http_curl("http://www.jingcaiweixinserver.51chongyin.com/v2/getPhotoSize",$params);
        $result = json_decode($result,true);
//        dump($result);die;
        $this->assign("list",$result) ;
        return $this->fetch("print_list");
    }

    public function albumlist()
    {
        if($this->store_id){
            $where['store_id'] = $this->store_id;
        }
        $where['type'] = PrintCategory::TYPE_ALBUM;
        $this->assign('list', $this->logicCategory->getPrintCategoryList($where));
        $this->assign("type",PrintCategory::TYPE_ALBUM);
        return $this->fetch('print_category_list');
    }

    public function calendarlist()
    {
        if($this->store_id){
            $where['store_id'] = $this->store_id;
        }
        $where['type'] = PrintCategory::TYPE_CALENDAR;
        $this->assign('list', $this->logicCategory->getPrintCategoryList($where));
        $this->assign("type",PrintCategory::TYPE_CALENDAR);
        return $this->fetch('print_category_list');
    }

    public function framelist()
    {
        if($this->store_id){
            $where['store_id'] = $this->store_id;
        }
        $where['type'] = PrintCategory::TYPE_FRAME;
        $this->assign('list', $this->logicCategory->getPrintCategoryList($where));
        $this->assign("type",PrintCategory::TYPE_FRAME);
        return $this->fetch('print_category_list');
    }

    public function posterlist()
    {
        if($this->store_id){
            $where['store_id'] = $this->store_id;
        }
        $where['type'] = PrintCategory::TYPE_POSTER;
        $this->assign('list', $this->logicCategory->getPrintCategoryList($where));
        $this->assign("type",PrintCategory::TYPE_POSTER);
        return $this->fetch('print_category_list');
    }


    /**
     * 文章分类删除
     */
    public function categoryDel($id = 0)
    {

        $this->jump($this->logicCategory->printCategoryDel(['id' => $id]));
    }
    
    /**
     * 数据状态设置
     */
    public function setStatus()
    {
        
        $this->jump($this->logicAdminBase->setStatus('PrintCategory', $this->param));
    }
}
