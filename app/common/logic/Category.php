<?php
// +---------------------------------------------------------------------+
// | JadePHP    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Xianfeng <107604242@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | JadePHP                      |
// +---------------------------------------------------------------------+

namespace app\common\logic;

/**
 * 冲印逻辑
 */
class Category extends LogicBase
{
    
    /**
     * 冲印分类编辑
     */
    public function printCategoryEdit($data = [])
    {
        
        $validate_result = $this->validatePrintCategory->scene('edit')->check($data);
        
        if (!$validate_result) {
            
            return [RESULT_ERROR, $this->validatePrintCategory->getError()];
        }
        $url = url('CategoryList',['type'=>$data['type']]);
        if(!is_administrator()){
            $member_info = session("member_info");
            $data['store_id'] = $member_info['store_id']?$member_info['store_id']:0;
        }else{
            unset($data['store_id']);
        }

        $result = $this->modelPrintCategory->setInfo($data);
        
        $handle_text = empty($data['id']) ? '新增' : '编辑';
        
        $result && action_log($handle_text, '冲印分类' . $handle_text . '，name：' . $data['name']);
        
        return $result ? [RESULT_SUCCESS, '操作成功', $url] : [RESULT_ERROR, $this->modelPrintCategory->getError()];
    }

    public function printClassEdit($data = [])
    {

        $validate_result = $this->validatePrintClass->scene('edit')->check($data);

        if (!$validate_result) {

            return [RESULT_ERROR, $this->validatePrintClass->getError()];
        }
        $url = url('PrintClassList',['category_id'=>$data['category_id']]);


        $result = $this->modelPrintClass->setInfo($data);

        $handle_text = empty($data['id']) ? '新增' : '编辑';

        $result && action_log($handle_text, '冲印规格' . $handle_text . '，name：' . $data['name']);

        return $result ? [RESULT_SUCCESS, '操作成功', $url] : [RESULT_ERROR, $this->modelPrintClass->getError()];
    }

    /**
     * 获取分类信息
     */
    public function getPrintCategoryInfo($where = [], $field = true)
    {
        
        return $this->modelPrintCategory->getInfo($where, $field);
    }
    public function getPrintClassInfo($where = [], $field = true)
    {

        return $this->modelPrintClass->getInfo($where, $field);
    }

    /**
     * 获取冲印分类列表
     */
    public function getPrintCategoryList($where = [], $field = true, $order = '', $paginate = 0)
    {
        
        return $this->modelPrintCategory->getList($where, $field, $order, $paginate);
    }

    /**
     * 获取冲印分类列表
     */
    public function getPrintClassList($where = [], $field = true, $order = '', $paginate = 0)
    {

        return $this->modelPrintClass->getList($where, $field, $order, $paginate);
    }


    /**
     * 冲印分类删除
     */
    public function printCategoryDel($where = [])
    {
        
        $result = $this->modelPrintCategory->deleteInfo($where);
        
        $result && action_log('删除', '冲印分类删除，where：' . http_build_query($where));
        
        return $result ? [RESULT_SUCCESS, '冲印分类删除成功'] : [RESULT_ERROR, $this->modelPrintCategory->getError()];
    }
    

}
