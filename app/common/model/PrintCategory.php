<?php
// +---------------------------------------------------------------------+
// | JadePHP    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Xianfeng <107604242@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | JadePHP                      |
// +---------------------------------------------------------------------+

namespace app\common\model;

/**
 * 文章分类模型
 */
class PrintCategory extends ModelBase
{
    CONST TYPE_ALBUM = "相册";
    CONST TYPE_CALENDAR = "台历";
    CONST TYPE_FRAME = "框画";
    CONST TYPE_POSTER = "海报";

    
}
