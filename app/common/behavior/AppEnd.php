<?php
// +---------------------------------------------------------------------+
// | JadePHP    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Xianfeng <107604242@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | JadePHP                      |
// +---------------------------------------------------------------------+

namespace app\common\behavior;

/**
 * 应用结束行为
 */
class AppEnd
{

    /**
     * 行为入口
     */
    public function run()
    {

        debug('app_end');
        
        write_exe_log('app_begin', 'app_end');
    }
}
