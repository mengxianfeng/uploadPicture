<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:78:"/data/wwwroot/hbb.zeekun.com/public/../app/admin/view/category/print_list.html";i:1527949125;}*/ ?>
<div class="box">

  <div class="box-body table-responsive">
    <table  class="table table-bordered table-hover">
      <thead>
      <tr>

          <th>显示规格</th>
          <th>门市价格（元）</th>
          <th>计价价格（元）</th>
          <th>照片宽度（cm）</th>
          <th>照片高度（cm）</th>
      </tr>
      </thead>
      
      <?php if(!(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty()))): ?>
        <tbody>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <tr>

                  <td><?php echo $vo['photodimension']; ?></td>
                  <td><?php echo $vo['price']; ?></td>
                  <td><?php echo $vo['couponPrice']; ?></td>
                  <td><?php echo $vo['xrate']; ?></td>
                  <td><?php echo $vo['yrate']; ?></td>

                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
        <?php else: ?>
        <tbody><tr class="odd"><td colspan="8" class="text-center" valign="top"><?php echo config('empty_list_describe'); ?></td></tr></tbody>
      <?php endif; ?>
    </table>
  </div>

</div>